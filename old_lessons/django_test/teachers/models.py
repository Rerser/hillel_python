from django.db import models
from django.db.models.deletion import SET_NULL

from faker import Faker
from courses.models import Course

from core.models import Person, get_obj

from random import randint


class Teacher(Person):
    salary = models.PositiveIntegerField(default=1500)

    subject = models.CharField(max_length=100, null=False)
    specialization = models.CharField(max_length=100)

    @classmethod
    def _generate(cls):
        fake = Faker()
        obj = super()._generate()
        obj.salary = randint(500, 3000)
        obj.specialization=fake.job(),
        obj.subject=get_obj(Course),
        obj.save()
        # print (get_obj(Course))


# Generating fake data (Teacher's objects)
# Teacher.generate(10)
