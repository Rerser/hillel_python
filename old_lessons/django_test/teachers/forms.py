from django.core.exceptions import ValidationError
from django.forms import ModelForm

import django_filters

from teachers.models import Teacher


class TeacherCreateForm(ModelForm):
    class Meta:
        model = Teacher
        fields = [
            'last_name', 
            'first_name', 
            'subject', 
            'specialization'
            ]


class TeacherUpdateForm(TeacherCreateForm):
    class Meta(TeacherCreateForm.Meta):
        fields = [
            "last_name",
            "first_name",
            "subject",
            "specialization",
        ]


class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = {
            'subject': ['exact', 'icontains'],
            'first_name': ['exact', 'icontains'],
            'last_name': ['exact', 'icontains'],
        }
