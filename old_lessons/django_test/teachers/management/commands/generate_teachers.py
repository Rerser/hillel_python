from django.core.management.base import BaseCommand

from teachers.models import Teacher


class Command(BaseCommand):
    help = "Teachers generator"  # noqa

    def add_arguments(self, parser):
        parser.add_argument(
            'n',
            nargs='?',
            default=10,
            type=int
        )
        return super().add_arguments(parser)

    def handle(self, *args, **options):
        number = options['n']
        Teacher.generate(count=number)
        self.stdout.write(self.style.SUCCESS('Successfully add "%s" teachers' % number))
