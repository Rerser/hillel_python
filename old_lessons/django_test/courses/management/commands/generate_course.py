from django.core.management.base import BaseCommand

from courses.models import Course

class Command(BaseCommand):
    help = "Groups generator"  # noqa

    def add_arguments(self, parser):
        parser.add_argument(
            'n',
            nargs='?',
            default=10,
            type=int
        )
        return super().add_arguments(parser)

    def handle(self, *args, **options):
        number = options['n']
        Course.generate_course(number)
        self.stdout.write(self.style.SUCCESS('Successfully add "%s" course' % number))
