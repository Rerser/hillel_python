from courses.views import CourseListView, CreateCourseView, CourseUpdateView, delete_Course

from django.urls import path

app_name = 'courses'

urlpatterns = [
    path('', CourseListView.as_view(), name='list'),
    path('create/', CreateCourseView.as_view(), name='create'),
    path('update/<int:pk>/', CourseUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', delete_Course, name='delete'),
]
