from django.urls import path

from .views import CreateStudentView, StudentDeleteView, StudentListView, StudentUpdateView

app_name = 'students'

urlpatterns = [
    # path('', get_students, name='list'),
    # path('create/', create_student, name='create'),
    # path('update/<int:id>/', update_student, name='update'),
    # path('update/<int:pk>/', UpdateStudentView.update_object, name='update'),

    path ('', StudentListView.as_view(), name='list'),
    path ('create/', CreateStudentView.as_view(), name='create'),
    path('update/<int:pk>/', StudentUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', StudentDeleteView.as_view(), name='delete'),
]
