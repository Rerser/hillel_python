import datetime
from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker

from groups.models import Group
from core.models import Person, get_obj

from dateutil.relativedelta import relativedelta

# Create your models here.
from students.validators import adult_validator, AdultValidator


class Students(Person):
    enroll_date = models.DateField(default=datetime.date.today)
    graduate_date = models.DateField(default=datetime.date.today)

    group = models.ForeignKey(
        Group, 
        on_delete=models.SET_NULL, 
        null=True, 
        related_name='students'
        )

    def __str__(self):
        return f'{self.full_name()}, {self.birthdate}, {self.id}, {self.group}'
    
    @classmethod
    def _generate(cls):
        faker = Faker()
        obj = super()._generate()
        obj.enroll_date = faker.date_between(start_date='-5y', end_date='-2y'),
        obj.graduate_date = faker.date_between(start_date='today', end_date='+3y'),
        obj.group = get_obj(Group)
        obj.save()
        # print (get_obj(Course))

