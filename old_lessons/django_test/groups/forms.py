from django.forms import ModelForm

import django_filters

from groups.models import Group


class GroupCreateForm(ModelForm):
    class Meta:
        model = Group
        fields = ['group_name', 'subject', 'creation_date']


class GroupUpdateForm(GroupCreateForm):
    class Meta(GroupCreateForm.Meta):
        fields = [
            'group_name',
            'subject',
            'creation_date',
            'headman',
            'teachers'
        ]


class GroupFilter(django_filters.FilterSet):
    class Meta:
        model = Group
        fields = {
            'subject': ['exact','icontains'],
            'group_name': ['exact', 'icontains'],
        }
