from django.core.management.base import BaseCommand

from groups.models import Group

class Command(BaseCommand):
    help = "Groups generator"  # noqa

    def add_arguments(self, parser):
        parser.add_argument(
            'n',
            nargs='?',
            default=10,
            type=int
        )
        return super().add_arguments(parser)

    def handle(self, *args, **options):
        number = options['n']
        Group.generate_groups(number)
        self.stdout.write(self.style.SUCCESS('Successfully add "%s" group' % number))
