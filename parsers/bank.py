from typing import List

from abc import ABC, abstractmethod


class Bank(ABC):

    # def __init__(self, currencies: List, bank_url: str, bank_id: int):
    #     self.__currencies = {
    #         element[2]: (element[0], element[1]) for element in currencies
    #     }
    #     self.__bank_url = bank_url
    #     self.__bank_id = bank_id

    @abstractmethod
    def get_currency_rate(self):
        pass
