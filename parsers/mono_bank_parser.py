from typing import List
import requests

from parsers.bank import Bank

class MonoBankParser(Bank):

    _CODES =  {840:'USD', 643:'RUB', 978:'EUR'}

    def __init__(self, currencies: List, bank_url: str, bank_id: int):
        self.__currencies = {
            element[2]: (element[0], element[1]) for element in currencies
        }
        self.__bank_url = bank_url
        self.__bank_id = bank_id

    def __get_json(self):
        resp = requests.get(self.__bank_url)
        return resp.json()

    def get_currency_rate(self):
        currency_rate = {
            'bank_id': self.__bank_id,
            'rate': []
        }

        result = self.__get_json()
        
        for line in result:
            newDict = line  # TypeError: string indices must be integers
            if newDict['currencyCodeB'] == 980 and newDict['currencyCodeA'] in self._CODES.keys():
                code = newDict['currencyCodeA']
                currency_rate['rate'].append(
                     {
                         'currency_id': self.__currencies.get(self._CODES.get(code).lower())[0],
                         'purchase': round(float(newDict['rateBuy']), 2),
                         'sale': round(float(newDict['rateSell']), 2)
                     }
                )

        return currency_rate


if __name__ == '__main__':
    from connector import DbUtils
    from pprint import pprint

    db = DbUtils()
    db.connect()
    currencies = db.get_currencies()
    bank_id, bank_name, bank_url = db.get_bank_by_id(5)
    db.close()
    parser = MonoBankParser(currencies, bank_url, bank_id)
    pprint(parser.get_currency_rate())
